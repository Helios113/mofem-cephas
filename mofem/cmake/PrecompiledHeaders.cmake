# Copyright (C) 2016, Lukasz Kaczmarczyk (likask AT wp.pl)
# The MoFEM package is copyrighted by Lukasz Kaczmarczyk.
# It can be freely used for educational and research purposes
# by other institutions. If you use this softwre pleas cite my work.
#
# MoFEM is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# MoFEM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MoFEM. If not, see <http://www.gnu.org/licenses/>

option(NOT_PRECOMPILED_HEADRES "Do not use procompiled headers" OFF)
if(NOT NOT_PRECOMPILED_HEADRES)
  if(${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}.${CMAKE_PATCH_VERSION} VERSION_GREATER 3.0.0)
    if("${CMAKE_CXX_COMPILER_ID}" MATCHES ".*[Cc]lang" OR "${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU")
      set(PRECOMPILED_HEADRES ON)
    endif("${CMAKE_CXX_COMPILER_ID}" MATCHES ".*[Cc]lang" OR "${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU")
  endif(${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}.${CMAKE_PATCH_VERSION} VERSION_GREATER 3.0.0)
endif(NOT NOT_PRECOMPILED_HEADRES)

function(my_add_executable target source)
  if(PRECOMPILED_HEADRES)
    set_source_files_properties(${source} PROPERTIES COMPILE_FLAGS "-include ${PERCOMPILED_MOFEM_HEADER}")
  endif(PRECOMPILED_HEADRES)
  add_executable(${target} ${source})
  if(PRECOMPILED_HEADRES)
    add_dependencies(${target} MoFEM.hpp.pch_copy)
  endif(PRECOMPILED_HEADRES)
endfunction(my_add_executable)
